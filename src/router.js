import Vue from 'vue'
import Router from 'vue-router'

const Container = () => import('@/views/containers/layout')
const Starships = () => import('@/views/pages/starships/index')

Vue.use(Router)

export default new Router({
  mode: 'history',
  linkActiveClass: 'active',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/starships',
      name: 'Starships',
      component: Container,
      children: [
        {
          path: 'starships',
          name: 'Starships',
          component: Starships
        }
      ]
    }
  ]
})
